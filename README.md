# Wren Nest

An opinionated commandline interpreter for the wren programming language. 

## About

An implementation of the wren commandline interpreter providing slightly better
include support and shipping with its own core library.

## Requirements

* libmath
* libwren
* libmagic
* scons
* c99 compatible compiler
* Posix os (Sorry windows users)

class HTTPMethod {
    HEAD { "HEAD" }
    
    GET { "GET" }

    POST { "POST" }

    PUT { "PUT" }

    DELETE { "DELETE" }

    OPTIONS { "OPTIONS" }
}

class HttpHeader {
    construct new(name, value) {
        _name = name
        _value = value
    }

    name { _name }

    value { _value }
}

class Scheme {
    HTTP { "http" }

    HTTPS { "https" }

    FTP { "FTP" }
}

class UrlParser {
    construct new() {
        _ports = {
            "http": "80",
            "https": "433",
            "ftp": "22"
        }

        _validSchemes = [
            "http",
            "https",
            "ftp"
        ]
    }

    /*
     * Checks the the provided @scheme is a valid and supported scheme
     * @param string scheme
     * @return bool
     */
    validateScheme(scheme) {
        if (!_validSchemes.contains(scheme)) {
            Fiber.abort("Invalid scheme provided")
        }

        return scheme
    }

    /*
     * Validates the username and password section of a given uri
     * @param string userPasswordString
     */
    validateUsernameAndPassword(userPassString) {
        if (!userPassString.contains(":")) {
            _username = userPassString
        } else {
            var split = userPassString.split(":")
            _username = split[0]
            _password = split[1]
        }
    }

    validateHostAndPort(hostPortString, parsed) {
        var host = false
        var port = false

        if (hostPortString.contains(":")) {
            var split = hostPortString.split(":")
            host = split[0]
            port = split[1]
        } else {
            host = hostPortString
            port = _ports[parsed["scheme"]]
        }

        parsed["host"] = host
        parsed["port"] = port
    }

    validatePath(path, parsed) {
        var p = false
        var query = false

        if (path.contains("?")) {
            var split = path.split("?")
            p = split[0]
            query = split[1]
        } else {
            p = path
        }

        parsed["path"] = p
        parsed["query"] = query
    }

    /*
     * Parses a given @uri into it's component parts
     * @param string uri
     * @throws Fiber
     */
    parse(uri) {
        var parsed = {
            "scheme": false,
            "username": false,
            "password": false,
            "host": false,
            "port": false,
            "path": false,
            "query": false,
            "fragment": false
        }

        //<scheme>//<user>:<password>@<host>:<port>/<path>#<fragment>
        var index = uri.indexOf(":")

        if (index == -1) {
            Fiber.abort("Missing scheme from uri") 
        }

        // Validate the scheme
        var scheme = uri[0 .. (index - 1)]
        parsed["scheme"] = this.validateScheme(scheme)

        index = index + 1

        while (uri[index] == "/") {
            index = index + 1
        }

        //var ampIndex = uri.indexOf("@", index)

        // If provided, validate the provided username and password
        //if (ampIndex != -1) {
        //    var userPassString = uri[index .. (ampIndex - 1)]
        //    this.validateUsernameAndPassword(userPassString)
        //    index = ampIndex + 1
        //}

        var rest = uri[index ... uri.count]

        var slashIndex = rest.indexOf("/")

        if (slashIndex == -1) {
            rest = rest + "/"
            slashIndex = rest.count
        }

        var host = rest[0 .. (slashIndex - 1)]
        this.validateHostAndPort(host, parsed)

        var path = rest[slashIndex ... rest.count]
        this.validatePath(path, parsed)

        System.print(parsed)

        return parsed
    }
}

class HttpClient {
    construct new() {
        _parser = UrlParser.new()    
    }

    foreign makeConnection(hostname, port)

    foreign sendRequest(conn, request)

    foreign recieveResponse(conn)

    buildRequest(method, parsedUrl, options) {
        var request = method + " " + parsedUrl["path"] + " HTTP/1.1\r\n"
        
        request = request + "Host: " + parsedUrl["host"] + "\r\n"
        request = request + "Connection: close\r\n"
        request = request + "\r\n"
        return request
    }

    parseResponse(data) {

    }

    doRequest(method, url, options) {
        var parsedUrl = _parser.parse(url)         
        var request = this.buildRequest(method, parsedUrl, {})
        var conn = this.makeConnection(
            parsedUrl["host"],
            parsedUrl["port"]
        )
        this.sendRequest(conn, request)
        
        var buffer = ""
        return this.recieveResponse(conn)
    }

    request(method, url) {
        return this.doRequest(method, url, {})
    }

    request(method, url, options) {
        return this.doRequest(method, url, options)
    }
}

/*
 * Generic class for performing io operations
 */
class IO {
    /*
     * Removes a file from @path
     * @param string path
     * @return bool
     */
    foreign static remove(path)

    /*
     * Renames a file or directory to a new name
     * @param string oldname
     * @param string newname
     * @return bool
     */
    foreign static rename(oldname, newname)

    /*
     * Copies a file for directory from one path to another
     * @param string source
     * @param string destination
     * @return bool
     */
    foreign static copy(source, destination)

    /*
     * Checks if a file exists at @path
     * @param string path
     * @return bool
     */
    foreign static fileExists(path)

    /*
     * Sets the permissions on a file
     * @param string path
     * @param string mode
     * @return bool
     */
    foreign static chmod(path, mode)

    /*
     * Sets the owner of a file at @path
     * @param string path
     * @param string owner
     * @return bool
     */
    foreign static chown(path, owner)

    /*
     * Sets the owner and group of a file at @path
     * @param string path
     * @param string owner
     * @param string group
     * @return bool
     */
    foreign static chown(path, owner, group)

    /*
     * Creates a directory at the given @path
     * @param string path
     * @return bool
     */
    foreign static mkdir(path)

    /*
     * Creates a directory at the given @path with the permissions of @mode
     * @param string path
     * @param string mode
     * @return bool
     */
    foreign static mkdir(path, mode)
}

/*
 * Wrapper class around the c FILE* variable
 */
foreign class File {
    /*
     * Opens a file at @path 
     * @param string path
     * @throws fiber - If class fails to open a file at @path
     */
    construct open(path) {}

    /*
     * Opens a new file at @path
     * @param string path
     * @param string mode
     * @throws fiber - If class fails to open a file at @path
     */
    construct open(path, mode) {}

    /*
     * Writes @data to an open file
     * @param string data
     */
    foreign write(data)

    /*
     * Read from a file
     * @param double size
     * @return string
     */
    foreign read(size)

    /*
     * Rewinds the file pointer to the start of the file
     */
    foreign rewind()

    /*  
     * Returns whether the file pointer is at the end of the file or not
     * @return bool
     */
    foreign eof()

    /*
     * Closes an open file
     */
    foreign close()
}

/*
 * Wrapper class around the c dir function
 */
foreign class Directory {
    /*
     * Opens a directory at @path
     * @param string path
     * @throws Fiber - If the constructor fails to open a directory at @path
     */
    construct open(path) {}

    /*
     * Reads the next entry in a directory. Returns null when it's reached the
     * end of the directory list
     * @return string|null
     */
    foreign read()

    /*
     * Rewinds the directory pointer to the first entry in the directory
     */
    foreign rewind()

    /*
     * Closes an open directory
     */
    foreign close()
}

/*
 * Wrapper class around the c stat struct
 */
foreign class Stat {
    /*
     * Constructor
     * @param string path
     * @abortsFiber - If the constructor fails to stat a file at @path
     */
    construct stat(path) {}

    /*
     * Returns the size of a file in bytes
     * @return double
     */
    foreign size()

    /*
     * Returns whether the stated path is a regular file or not
     * @return bool
     */
    foreign isFile()

    /*
     * Returns whether the stated path is a directory or not
     * @return bool
     */
    foreign isDirectory()

    /*
     * Returns whether the stated path is a symbolic link or not
     * @return bool
     */
    foreign isSymbolicLink()

    /*
     * Returns the last accessed time as a unix timestamp
     * @return double
     */
    foreign lastAccessedTime()

    /*
     * Returns the last modified time as a unix timestamp
     * @return double
     */
    foreign lastModifiedTime()
}

/*
 * Class for getting the mimetype for a file
 */
class Mime {
    /*
     * Gets the mimetype of a file at @path
     * @param string path
     * @return string
     */
    foreign static pathMimeType(path)

    /*
     * Gets the mimetype of a file from a file instance
     * @param File file
     * @return string
     */
    foreign static fileMimeType(file)
}

/*
 * Class for interacting with the environment
 */
class Environment
{
    /*
     * Gets the environment variable value of @key
     * @param string key
     * @return string|null
     */
    foreign static get(key)

    /*
     * Sets an environment variable called @key with the value @value
     * @param string key
     * @param string value
     * @return bool
     */
    foreign static set(key, value)

    /*
     * Gets the home directory of the user running the current program
     * @return string|null
     */
    static home()
    {
        return this.get("HOME")
    }
}

/**
 * Class for interacting with the system and wren vm
 */
class System
{
    /**
     * Calls the wren garbage collector
     */
    foreign static gc()

    /**
     * Immediately exits the wren program.
     * Unsafe as it doesn't clean up before exiting the code
     * @param double code
     */
    foreign static exit(code)

    /**
     * Immediately exits the wren program with a success code
     */
    foreign static exit()
}

class Output
{
    /**
     * Converts an object to it's string representation
     * @param object obj
     * @return string
     */
    objectToString(obj)
    {
        var string = obj.toString

        if (string is String) {
            return string
        }

        return "[invalid toString]"
    }

    /**
     * Converts a sequence to a string
     * @param Sequence sequence
     * @return string
     */
    sequenceToString(sequence)
    {
        var string = ""

        for (object in sequence) {
            string = string + this.objectToString(object)
        }

        return string
    }

    /**
     * Converts an array to a string
     * @param mixed data
     * @return string
     */
    convert(data)
    {
        if (data is Sequence) {
            return this.sequenceToString(data)
        }

        return this.objectToString(data)
    }

    print(data)
    {
        this.write(this.convert(data))
    }

    println(data)
    {
        this.write(this.convert(data) + "\n")
    }
}

/**
 * Class for writing to stdout
 */
class Stdout is Output
{
    construct new() {}

    /**
     * Writes to stdout
     * @param mixed data
     */
    foreign write(data)

    /**
     * Writes a single newline to stdout
     */
    println()
    {
        this.write("\n")
    }
}

/**
 * Class writing to stderr
 */
class Stderr is Output
{
    construct new() {}

    /**
     * Writes to stderr
     * @param mixed data
     */
    foreign write(data)
}

/* Create some global variables for writing to the console */
var stdout = Stdout.new()
var stderr = Stderr.new()

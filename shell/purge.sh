#!/bin/bash

BINARY="/usr/local/bin/wren"
MODULE_DIR="/usr/local/lib/wren"

if [ -f "$BINARY" ]; then
    rm -v "$BINARY"
fi

if [ -d "$MODULE_DIR" ]; then
    rm -rv "$MODULE_DIR"
fi

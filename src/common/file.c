#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include "file.h"

int fileExists(const char* path)
{
    struct stat st;

    if (stat(path, &st) < 0) {
        return 0;
    }

    return S_ISREG(st.st_mode);
}

size_t getFileSize(const char* path)
{
    struct stat st;

    if (stat(path, &st) < 0) {
        return -1;
    }

    return st.st_size;
}

char* readFile(const char* path)
{
    FILE* fp = fopen(path, "r");

    if (fp == NULL) {
        return NULL;
    }

    const size_t fileSize = getFileSize(path);

    if (fileSize < 0) {
        fclose(fp);
        return NULL;
    }

    char* buffer = (char*)malloc(fileSize + 1);

    if (buffer == NULL) {
        fclose(fp);
        return NULL;
    }

    const size_t bytesRead = fread(buffer, 1, fileSize, fp);

    buffer[bytesRead] = '\0';
    fclose(fp);
    return buffer;
}

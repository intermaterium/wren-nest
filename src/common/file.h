#ifndef FILE_H
#define FILE_H

#include <string.h>

/**
 * Checks if a file exists, and if so, that it is a regular file
 */
int fileExists(const char* path);

/**
 * Gets the size of a file
 */
size_t getFileSize(const char* path);

/**
 * Attempts to read the entireity of a file. Returns NULL if the file does not
 * exist or it fails to read the file.
 */
char* readFile(const char* path);

#endif

#define INITIAL_SIZE 255

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "hashMap.h"

static void freeNodeItem(struct HashMapNodeItem* data)
{
    free(data->key);
    free(data);
}

static struct HashMapNodeItem* createNodeItem(const char* key, any_t data)
{
    struct HashMapNodeItem* item = malloc(sizeof(struct HashMapNodeItem));

    if (item == NULL) {
        return NULL;
    }

    size_t len = strlen(key);
    item->key = malloc(len + 1);
    memcpy(item->key, key, len);
    item->key[len] = '\0';

    item->value = data;
    return item;
}

struct HashMap* hashMapNew()
{
    struct HashMap* map = malloc(sizeof(struct HashMap));

    if (map == NULL) {
        return NULL;
    }

    map->nodes = malloc(255 * sizeof(struct LinkedList));

    if (map->nodes == NULL) {
        free(map);
        return NULL;
    }

    for (int i = 0; i < 255; i++) {
        linkedListNew(&map->nodes[i]);
    }

    map->collissionCount = 0;
    map->capacity = INITIAL_SIZE;
    return map;
}

/* Return a 32-bit CRC of the contents of the buffer. */

static unsigned int hashKey(const char* keystring, size_t tableSize)
{
    unsigned long hash = 5381;
    int c;

    for (int i = 0; i < strlen(keystring); i++) {
        c = keystring[i];
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

	return hash % 255;
}


int hashMapInsert(struct HashMap* map, const char* key, any_t data)
{
    if (map == NULL || key == NULL || data == NULL) {
        return 1;
    }

    const unsigned int index = hashKey(key, map->capacity);
    struct LinkedList* list = &map->nodes[index];
    struct HashMapNodeItem* item = createNodeItem(key, data);

    /* if the linked list already has elements, treat it as a collission */
    if (linkedListLength(list) > 0) {
        map->collissionCount++;
    }

    return linkedListAppend(list, item);
}

any_t hashMapSearch(const struct HashMap* map, const char* key)
{
    if (map == NULL || key == NULL) {
        return NULL;
    }

    const unsigned int index = hashKey(key, map->capacity);
    struct LinkedList* list = &map->nodes[index];
    
    const size_t len = linkedListLength(list);

    switch (len) {
        case 0:
            return NULL;
        case 1: {
            struct HashMapNodeItem* item = linkedListHead(list);
            return item->value;
        }
        default: {
            struct HashMapNodeItem* item = NULL;

            while ((item = linkedListNext(list)) != NULL) {
                if (strcmp(item->key, key) == 0) {
                    linkedListRewind(list);
                    return item->value;
                }
            }

            linkedListRewind(list);
            return NULL;
        }
    }
}

void hashMapFree(struct HashMap* map)
{
    if (map == NULL) {
        return;
    }

    for (int i = 0; i < INITIAL_SIZE; i++) {
        struct LinkedList* list = &map->nodes[i];
        struct HashMapNodeItem* item = NULL;

        while ((item = linkedListNext(list)) != NULL) {
            freeNodeItem(item);
        }

        linkedListFree(list);
    }

    free(map->nodes);
    free(map);
}

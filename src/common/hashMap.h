#ifndef HASH_MAP_H
#define HASH_MAP_H

#include "linkedList.h"

struct HashMapNodeItem
{
    char* key;
    any_t value;
};

struct HashMap
{
    struct LinkedList* nodes; 
    int collissionCount;
    size_t capacity;
};

void hashMapFree(struct HashMap *map);

any_t hashMapSearch(const struct HashMap *map, const char *key);

int hashMapInsert(struct HashMap *map, const char *key, any_t data);

struct HashMap* hashMapNew();

#endif

#include "linkedList.h"
#include <stdlib.h>

void linkedListNew(struct LinkedList* list) 
{
    list->first = NULL;
    list->last = NULL;
    list->current = NULL;
    list->length = 0;
}

size_t linkedListLength(const struct LinkedList* list)
{
    return list->length;
}

any_t linkedListHead(const struct LinkedList* list)
{
    if (list->length > 0) {
        return list->first->data;
    }

    return NULL;
}

any_t linkedListLast(const struct LinkedList* list)
{
    if (list->length > 0) {
        return list->last->data;
    }

    return NULL;
}

static struct Item* createItem(const struct LinkedList* list, const any_t item)
{
    struct Item* tmp = malloc(sizeof(struct Item));

    if (tmp == NULL) {
        return NULL;
    }

    tmp->data = item;
    tmp->next = NULL;
    return tmp;
}

int linkedListAppend(struct LinkedList* list, const any_t item)
{
    if (item == NULL) {
        return 1;
    }

    struct Item* tmp = createItem(list, item);

    if (tmp == NULL) {
        return -1;
    }

    if (list->first == NULL) {
        list->first = tmp;
        list->last = tmp;
        list->current = tmp;
    } else {
        list->last->next = tmp;
        list->last = tmp;
    }

    list->length++;

    return 0;
}

int linkedListPreappend(struct LinkedList* list, const any_t item)
{
    if (item == NULL) {
        return 1;
    }

    struct Item* tmp = createItem(list, item);

    if (tmp == NULL) {
        return -1;
    }

    if (list->first == NULL) {
        list->first = tmp;
        list->last = tmp;
    } else {
        tmp->next = list->first;
        list->first = tmp;
    }

    list->length++;

    return 0;
}

any_t linkedListNext(struct LinkedList* list)
{
    if (list->current == NULL) {
        return NULL;
    }

    struct Item* res = list->current;
    list->current = list->current->next;
    return res->data;
}

void linkedListRewind(struct LinkedList* list)
{
    list->current = list->first;
}

void linkedListFree(struct LinkedList* list)
{
    for (struct Item* item = list->first; item != NULL;) {
        struct Item* tmp = item;
        item = tmp->next;
        free(tmp);
    }
}

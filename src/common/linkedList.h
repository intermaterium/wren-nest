#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <string.h>

typedef void* any_t;

struct Item
{
    struct Item* next;
    any_t data;
};
  
struct LinkedList
{
    struct Item* first;
    struct Item* last;
    struct Item* current;
    size_t length;
};

void linkedListFree(struct LinkedList* list);

void linkedListRewind(struct LinkedList* list);

any_t linkedListNext(struct LinkedList* list);

int linkedListPreappend(struct LinkedList* list, const any_t item);

int linkedListAppend(struct LinkedList* list, const any_t item);

any_t linkedListHead(const struct LinkedList* list);

any_t linkedListLast(const struct LinkedList* list);

size_t linkedListLength(const struct LinkedList* list);

void linkedListNew(struct LinkedList* list);

#endif

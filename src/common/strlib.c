#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "strlib.h"

char* cpystr(const char* source)
{
    const size_t len = strlen(source);
    char* dest = malloc(len + 1);
    memcpy(dest, source, len);
    dest[len] = '\0';
    return dest;
}

char* cpynstr(const char* source, size_t len)
{
    char* dest = malloc(len + 1);
    memcpy(dest, source, len);
    dest[len] = '\0';
    return dest;
}

int charcount(const char* str, char c)
{
    int count = 0;

    if (str == NULL) {
        return 0;
    }

    for (int i = 0; i < strlen(str); i++) {
        if (str[i] == c) {
            count++;
        }
    }

    return count;
}

char** strcsplit(
    const char* restrict str,
    const char* restrict delim
) {
    if (str == NULL) {
        return NULL;
    }

    int count = charcount(str, *delim);

    /* Add one to the count of delimiters to include characters after the final delimiter */
    count++;

    char** arr = malloc(sizeof(char*) * count + 1);

    const size_t length = strlen(str);
    char* token = strpbrk(str, delim);
    int i = 0;
    int pos = 0;

    while (token != NULL) {
        const int len = token - (str + pos);
        arr[i] = cpynstr((str + pos), len);
        pos += len + 1;
        token = strpbrk(token + 1, delim);
        i++;
    }

    if (pos < length) {
        arr[i] = cpystr(str + pos);
    }
    
    /* 
     * Null out the last element for that we know we have reached the end of the
     * array while looping
     */
    arr[count + 1] = NULL; 

    return arr;
}

int strisempty(const char* str)
{
    return (strcmp(str, "") == 0);
}

char* buildString(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    va_list args2;
    va_copy(args2, args);

    const size_t len = vsnprintf(NULL, 0, fmt, args2) + 1;
    va_end(args2);

    char* path = malloc(len);
    vsnprintf(path, len, fmt, args);
    va_end(args);

    return path;
}

char* strreplace(
    const char* restrict in,
    const char* restrict pattern,
    const char* restrict by
) {
    if (in == NULL || pattern == NULL || by == NULL) {
        return NULL;
    }

    size_t outsize = strlen(in) + 1;
    // TODO maybe avoid reallocing by counting the non-overlapping occurences of pattern
    char* res = malloc(outsize);

    if (res == NULL) {
        return NULL;
    }

    // use this to iterate over the output
    size_t resoffset = 0;

    char *needle;
    while ((needle = strstr(in, pattern)) != NULL) {
        // copy everything up to the pattern
        memcpy(res + resoffset, in, needle - in);
        resoffset += needle - in;

        // skip the pattern in the input-string
        in = needle + strlen(pattern);

        // adjust space for replacement
        outsize = outsize - strlen(pattern) + strlen(by);
        res = realloc(res, outsize);

        // copy the pattern
        memcpy(res + resoffset, by, strlen(by));
        resoffset += strlen(by);
    }

    // copy the remaining input
    strcpy(res + resoffset, in);

    return res;
}

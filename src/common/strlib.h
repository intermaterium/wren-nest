#ifndef STRLIB_H
#define STRLIB_H

char* cpystr(const char* source);

char* cpynstr(const char* source, size_t len);

int charcount(const char* str, char c);

char** strcsplit(
    const char* restrict str, 
    const char* restrict delim
);

int strisempty(const char* str);

char* buildString(const char* fmt, ...);

char* strreplace(
    const char* restrict in,
    const char* restrict pattern,
    const char* restrict by
);

#endif

#ifndef H_NEST
#define H_NEST

#include <wren.h>

struct NestClass
{
    char* name;
    WrenForeignClassMethods methods;
};

typedef struct NestClass NestClass;

struct NestMethod
{
    char* className;
    char* signature;
    bool isStatic;
    WrenForeignMethodFn func;
};

typedef struct NestMethod NestMethod;

struct NestLibrary
{
    NestClass* classes;
    NestMethod* methods;
};

typedef struct NestLibrary NestLibrary;

/**
 * Causes wren to abort the fiber with a given @error message
 */
inline void nestThrowError(WrenVM* vm, const char* error)
{
    wrenSetSlotString(vm, 0, error);
    wrenAbortFiber(vm, 0); 
} 

#endif

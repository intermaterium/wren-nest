#define _POSIX_C_SOURCE 200809L

#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include "../nestCore.h"
#include "../strlib.h"

#define CHUNK_SIZE 500

/**
 * Attempts to resolve a string hostname and port to an actual server connection
 */
static int resolveHost(
    const char* restrict hostname,
    const char* restrict port,
    struct addrinfo** res
) {
    struct addrinfo hints;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    return getaddrinfo(hostname, port, &hints, res);
}

/**
 * Attempts to make a connection with the resolved host on the port
 */
static int makeConnection(struct addrinfo* res)
{
    int sockfd;

    if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0) {
        return -1;
    }

    if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {
        return -1;
    }

    return sockfd;
}

static void httpClient_makeConnection(WrenVM* vm)
{
    const char* hostname = wrenGetSlotString(vm, 1);
    const char* port = wrenGetSlotString(vm, 2);

    struct addrinfo* addr;
    int res = resolveHost(hostname, port, &addr);

    if (res < 0) {
        char* error = buildString(
            "Error resolving host %s. Reason: %s", 
            hostname, 
            gai_strerror(res)
        );
        nestThrowError(vm, error);
        free(error);
        return;
    }

    int sockfd = makeConnection(addr);
    freeaddrinfo(addr);

    if (sockfd < 0) {
        char* error = buildString(
            "Error connecting to host %s on port %s. Reason: %s",
            hostname,
            port, 
            strerror(errno)
        );
        nestThrowError(vm, error);
        free(error);
        return;
    }

    wrenSetSlotDouble(vm, 0, sockfd);
}

static void httpClient_sendRequest(WrenVM* vm)
{
    int sockfd = wrenGetSlotDouble(vm, 1);
    const char* buffer = wrenGetSlotString(vm, 2);

    size_t bufferLength = strlen(buffer);
    size_t bytesSent = 0;

    while (bytesSent < bufferLength) {
        int sent = send(sockfd, buffer + bytesSent, CHUNK_SIZE, 0);

        if (sent < 0) {
            char* error = buildString("Failed to send request. Reason: %s", strerror(errno));
            nestThrowError(vm, error);
            free(error);
        }

        bytesSent += sent;
    }
}

static void httpClient_recieveResponse(WrenVM* vm)
{
    int sockfd = wrenGetSlotDouble(vm, 1);

    char data[500] = {0};

    //while (1) {
        int res = recv(sockfd, data, 499, 0);

        if (res == -1) {
            return;
        } else if (res == 0) {
            return;
        }

    //}

    wrenSetSlotString(vm, 0, data);
    close(sockfd);
}

NestClass classes[] = {
    {"HttpClient", {NULL, NULL}},
    {NULL, {NULL, NULL}}
};

NestMethod methods[] = {
    {"HttpClient", "makeConnection(_,_)", false, httpClient_makeConnection},
    {"HttpClient", "sendRequest(_,_)", false, httpClient_sendRequest},
    {"HttpClient", "recieveResponse(_)", false, httpClient_recieveResponse},
    {NULL, NULL, false, NULL}

};

NestLibrary nest_http_init()
{
    NestLibrary lib = {
        classes,
        methods
    };

    return lib;
}

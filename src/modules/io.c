#include "nest.h"
#include "strlib.h"
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <errno.h>
#include <magic.h>
#include <pwd.h>
#include <sys/types.h>

static const size_t MIME_BUFFER = 255;
static magic_t magic = NULL;
const char* DEFAULT_FILE_MODE = "r";

/**
 * Resets the stream file pointer to the start of the stream
 */
static void rewindFile(FILE* file)
{
    /* Only rewind the file if we are not at the start of the file already */
    if (ftell(file) > 0) {
        rewind(file);
    }
}

/**
 * Module for io operations in the wren language
 */

/**
 * Removes a file or directory from the filesystem
 */
static void io_remove(WrenVM* vm)
{
    const char* path = wrenGetSlotString(vm, 1);
    wrenSetSlotBool(vm, 0, remove(path) == 0);
}

/**
 * Ranames a file or directory
 */
static void io_rename(WrenVM* vm)
{
    const char* oldpath = wrenGetSlotString(vm, 1);
    const char* newpath = wrenGetSlotString(vm, 2);
    wrenSetSlotBool(vm, 0, rename(oldpath, newpath) == 0);
}

/**
 * Copies a file or directory from one location to another
 */
static void io_copy(WrenVM* vm)
{
    const char* source = wrenGetSlotString(vm, 1);
    const char* destination = wrenGetSlotString(vm, 2);

    int sourcefd = 0;
    int destfd = 0;

    if ((sourcefd = open(source, O_RDONLY)) < 0) {
        wrenSetSlotBool(vm, 0, 0);
        return;
    }

    if ((destfd = creat(destination, 0660)) < 0) {
        close(sourcefd);
        wrenSetSlotBool(vm, 0, 0);
        return;
    }

    struct stat st;
    fstat(sourcefd, &st);
    off_t bytesCopied = 0;

    const int result = sendfile(destfd, sourcefd, &bytesCopied, st.st_size);

    close(sourcefd);
    close(destfd);

    wrenSetSlotBool(vm, 0, (result == 0));
}

static void io_fileExists(WrenVM* vm)
{
    const char* path = wrenGetSlotString(vm, 1);
    struct stat st;
    wrenSetSlotBool(vm, 0, stat(path, &st) == 0);
}

static void io_chmod(WrenVM* vm)
{
    const char* path = wrenGetSlotString(vm, 1);
    const char* mode = wrenGetSlotString(vm, 2);
    const int m = strtoul(mode, 0, 8);
    wrenSetSlotBool(vm, 0, chmod(path, m) == 0);
}

static void io_chown(WrenVM* vm)
{
    struct passwd* pwd;
    uid_t uid = -1;
    gid_t gid = -1;

    const char* path = wrenGetSlotString(vm, 1);

    if (wrenGetSlotType(vm, 2) == WREN_TYPE_NUM) {
        uid = (int)wrenGetSlotDouble(vm, 2);
    } else {
        const char* owner = wrenGetSlotString(vm, 2);
        pwd = getpwnam(owner);
        uid = pwd->pw_uid;
    }

    if (wrenGetSlotCount(vm) == 4) {
        if (wrenGetSlotType(vm, 3) == WREN_TYPE_NUM) {
            gid = (int)wrenGetSlotDouble(vm, 3);
        } else {
            const char* group = wrenGetSlotString(vm, 3);
            pwd = getpwnam(group);
            gid = pwd->pw_gid;
        }
    }

    wrenSetSlotBool(vm, 0, chown(path, uid, gid) == 0);
}

static void io_mkdir(WrenVM* vm)
{
    mode_t mode = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH;
    const char* path = wrenGetSlotString(vm, 1);

    if (wrenGetSlotCount(vm) == 3) {
        const char* strmode = wrenGetSlotString(vm, 2);
        mode = strtoul(strmode, 0, 8);
    }

    wrenSetSlotBool(vm, 0, (mkdir(path, mode) == 0));
}

static inline bool checkFileMode(const char* mode)
{
    const size_t len = strlen(mode);
    
    if (len < 1) {
        return false;
    }

    const char c = mode[0];

    if (c != 'r' && c != 'w' && c != 'a') {
        return false;
    }

    switch (len) {
        case 1:
            return true;
        case 2:
            return (mode[1] == 'b' || mode[1] == '+');
        case 3:
            return ((mode[1] == 'b' && mode[2] == '+') || (mode[1] == '+' && mode[2] == 'b'));
        default:
            return false;
    }
}

/**
 * Initialiser function for the file class
 */
static void file_initialise(WrenVM* vm) 
{ 
    const char* path = wrenGetSlotString(vm, 1); 
    const char* mode = (wrenGetSlotCount(vm) == 3) ? 
        wrenGetSlotString(vm, 2) : DEFAULT_FILE_MODE;

    if (!checkFileMode(mode)) {
        char* error = buildString("Failed to open file %s. Reason: Invalid file mode %s", path, mode);
        nestThrowError(vm, error);
        free(error);
        return;
    }

    FILE* file = fopen(path, mode); 

    if (file == NULL) {
        char* error = buildString("Failed to open file %s. Reason: %s", path, strerror(errno));
        nestThrowError(vm, error);
        free(error);
        return;
    }

    FILE** fp = (FILE**)wrenSetSlotNewForeign(vm, 0, 0, sizeof(FILE*)); 
    *fp = file;
}

static void file_write(WrenVM* vm)
{
    FILE** file = (FILE**)wrenGetSlotForeign(vm, 0);

    if (file == NULL) {
        nestThrowError(vm, "Cannot write to a closed file");
        return;
    }

    const char* text = wrenGetSlotString(vm, 1); 
    const int bytesWritten = fwrite(text, sizeof(char), strlen(text), *file); 
    wrenSetSlotDouble(vm, 0, bytesWritten);
}

static void file_read(WrenVM* vm)
{
    FILE** file = (FILE**)wrenGetSlotForeign(vm, 0);

    if (file == NULL) {
        nestThrowError(vm, "Cannot read from a closed file");
        return;
    }

    const int bytes = wrenGetSlotDouble(vm, 1); 

    char* buffer = malloc(bytes + 1);
    const size_t res = fread(buffer, sizeof(char), bytes, *file); 
    buffer[res] = '\0';
    wrenSetSlotString(vm, 0, buffer);
    free(buffer);
}

static void file_rewind(WrenVM* vm)
{
    FILE** file = (FILE**)wrenGetSlotForeign(vm, 0);

    if (file == NULL) {
        nestThrowError(vm, "Cannot rewind a closed file");
        return;
    }

    rewindFile(*file);
}

static void file_eof(WrenVM* vm)
{
    FILE** file = (FILE**)wrenGetSlotForeign(vm, 0);

    if (file == NULL) {
        nestThrowError(vm, "Cannot check a closed file");
        return;
    }

    wrenSetSlotBool(vm, 0, (feof(*file) != 0));
}

static void closeFile(FILE** file)
{
    if (file == NULL) {
        return;
    }

    fclose(*file);
    file = NULL;
}

static void file_close(WrenVM* vm)
{
    FILE** file = (FILE**)wrenGetSlotForeign(vm, 0);
    closeFile(file);
}

/**
 * Finaliser function for the file class
 */
static void file_finalise(void* data)
{
    closeFile((FILE**)data);
}

static void directory_initialise(WrenVM* vm)
{
    const char* path = wrenGetSlotString(vm, 1);
    DIR* dir = opendir(path);

    if (dir == NULL) {
        char* error = buildString(
            "Failed to open directory %s. Reason: %s",
            path,
            strerror(errno)
        );
        nestThrowError(vm, error);
        free(error);
        return;
    }

    DIR** dp = (DIR**)wrenSetSlotNewForeign(vm, 0, 0, sizeof(DIR*)); 
    *dp = dir;
}

static void directory_read(WrenVM* vm)
{
    DIR** dir = (DIR**)wrenGetSlotForeign(vm, 0);

    if (dir == NULL) {
        nestThrowError(vm, "Cannot read from a closed directory");
        return;
    }

    const struct dirent* item = readdir(*dir);

    if (item == NULL) {
        wrenSetSlotNull(vm, 0);
    } else {
        wrenSetSlotString(vm, 0, item->d_name);
    }
}

static void directory_rewind(WrenVM* vm)
{
    DIR** dir = (DIR**)wrenGetSlotForeign(vm, 0);

    if (dir == NULL) {
        nestThrowError(vm, "Cannot rewind a closed directory");
        return;
    }

    rewinddir(*dir);
}

static void closeDirectory(DIR** dir)
{
    if (dir == NULL) {
        return;
    }

    closedir(*dir);
}

static void directory_close(WrenVM* vm)
{
    DIR** dir = (DIR**)wrenGetSlotForeign(vm, 0);
    closeDirectory(dir);
}

static void directory_finalise(void* data)
{
    closeDirectory((DIR**)data);   
}

static void stat_initialise(WrenVM* vm)
{
    const char* path = wrenGetSlotString(vm, 1); 
    struct stat st;
    
    if (stat(path, &st) < 0) {
        char* error = buildString(
            "Could not stat file %s. Reason: %s",
            path,
            strerror(errno)
        );
        nestThrowError(vm, error);
        free(error);
        return;
    }

    struct stat* st2 = (struct stat*)wrenSetSlotNewForeign(vm, 0, 0, sizeof(struct stat)); 

    memcpy(st2, &st, sizeof(struct stat));
}

static void stat_size(WrenVM* vm)
{
    struct stat* st = (struct stat*)wrenGetSlotForeign(vm, 0);
    wrenSetSlotDouble(vm, 0, st->st_size);
}

static void stat_isFile(WrenVM* vm)
{
    struct stat* st = (struct stat*)wrenGetSlotForeign(vm, 0);
    wrenSetSlotBool(vm, 0, S_ISREG(st->st_mode));
}

static void stat_isDirectory(WrenVM* vm)
{
    struct stat* st = (struct stat*)wrenGetSlotForeign(vm, 0);
    wrenSetSlotBool(vm, 0, S_ISDIR(st->st_mode));
}

static void stat_isSymbolicLink(WrenVM* vm)
{
    struct stat* st = (struct stat*)wrenGetSlotForeign(vm, 0);
    wrenSetSlotBool(vm, 0, S_ISLNK(st->st_mode));
}

static void stat_lastAccessedTime(WrenVM* vm)
{
    struct stat* st = (struct stat*)wrenGetSlotForeign(vm, 0);
    wrenSetSlotDouble(vm, 0, st->st_atime);
}

static void stat_lastModifiedTime(WrenVM* vm)
{
    struct stat* st = (struct stat*)wrenGetSlotForeign(vm, 0);
    wrenSetSlotDouble(vm, 0, st->st_mtime);
}

static bool initialiseMimeLibrary(WrenVM* vm)
{
    if ((magic = magic_open(MAGIC_MIME_TYPE | MAGIC_ERROR)) == NULL) {
        nestThrowError(vm, "Failed to initialise mime library");
        return false;
    }

    magic_load(magic, NULL);

    return true;
}

static void mime_pathMimeType(WrenVM* vm)
{
    if (magic == NULL && !initialiseMimeLibrary(vm)) {
        return;
    }

    const char* path = wrenGetSlotString(vm, 1);
    const char* mime = magic_file(magic, path);

    if (mime == NULL) {
        char* error = buildString("Failed to get file mime type. Reason: %s", magic_error(magic));
        nestThrowError(vm, error);
        free(error);
        return;
    }

    wrenSetSlotString(vm, 0, mime);
}

static void mime_fileMimeType(WrenVM* vm)
{
    if (magic == NULL && !initialiseMimeLibrary(vm)) {
        return;
    }

    FILE** file = (FILE**)wrenGetSlotForeign(vm, 1);

    /* rewind the file if we are not at the start of the file */
    rewindFile(*file);

    char buffer[MIME_BUFFER];
    const size_t read = fread(buffer, sizeof(char), MIME_BUFFER, *file);
    const char* mime = magic_buffer(magic, buffer, read);

    if (mime == NULL) {
        char* error = buildString(
            "Failed to get file mime type. Reason: %s",
            magic_error(magic)
        );
        nestThrowError(vm, error);
        free(error);
        return;
    }

    wrenSetSlotString(vm, 0, mime);
}

static NestClass classes[] = {
    {"IO", {NULL, NULL}},
    {"File", {file_initialise, file_finalise}},
    {"Directory", {directory_initialise, directory_finalise}},
    {"Stat", {stat_initialise, NULL}},
    {"Mime", {NULL, NULL}},
    {NULL, {NULL, NULL}}

};

static NestMethod methods[] = {
    {"IO", "remove(_)", true, io_remove},
    {"IO", "rename(_,_)", true, io_rename},
    {"IO", "copy(_,_)", true, io_copy},
    {"IO", "fileExists(_)", true, io_fileExists},
    {"IO", "chmod(_,_)", true, io_chmod},
    {"IO", "chown(_,_)", true, io_chown},
    {"IO", "chown(_,_,_)", true, io_chown},
    {"IO", "mkdir(_)", true, io_mkdir},
    {"IO", "mkdir(_,_)", true, io_mkdir},

    {"File", "write(_)", false, file_write},
    {"File", "read(_)", false, file_read},
    {"File", "rewind()", false, file_rewind},
    {"File", "eof()", false, file_eof},
    {"File", "close()", false, file_close},

    {"Directory", "read()", false, directory_read},
    {"Directory", "rewind()", false, directory_rewind},
    {"Directory", "close()", false, directory_close},

    {"Mime", "pathMimeType(_)", true, mime_pathMimeType},
    {"Mime", "fileMimeType(_)", true, mime_fileMimeType},

    {"Stat", "size()", false, stat_size},
    {"Stat", "isFile()", false, stat_isFile},
    {"Stat", "isDirectory()", false, stat_isDirectory},
    {"Stat", "isSymbolicLink()", false, stat_isSymbolicLink},
    {"Stat", "lastAccessedTime()", false, stat_lastAccessedTime},
    {"Stat", "lastModifiedTime()", false, stat_lastModifiedTime},

    {NULL, NULL, NULL, false}
};


NestLibrary nest_io_init()
{
    NestLibrary iolib = {
        classes,
        methods
    };

    return iolib;
}

void nest_io_finalise()
{
    if (magic != NULL) {
        magic_close(magic);
    }
}

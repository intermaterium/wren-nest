#define _BSD_SOURCE

#include "nest.h"
#include <stdio.h>
#include <stdlib.h>
#include "strlib.h"

static void environment_get(WrenVM* vm)
{
    const char* key = wrenGetSlotString(vm, 1);
    const char* env = getenv(key);

    if (env != NULL) {
        wrenSetSlotString(vm, 0, env);
    } else {
        wrenSetSlotNull(vm, 0);
    }
}

static void environment_set(WrenVM* vm)
{
    const char* key = wrenGetSlotString(vm, 1);
    const char* value = wrenGetSlotString(vm, 2);

    if (key == NULL) {
        return;
    }

    wrenSetSlotBool(vm, 0, (setenv(key, value, 1) == 0));
}

static void environment_unset(WrenVM* vm)
{
    const char* key = wrenGetSlotString(vm, 1);

    if (strisempty(key)) {
        return;
    }

    wrenSetSlotBool(vm, 0, (unsetenv(key) == 0));
}

static void system_gc(WrenVM* vm)
{
    wrenCollectGarbage(vm);
}

static void system_exit(WrenVM* vm)
{
    int code = EXIT_SUCCESS; 
    
    if (wrenGetSlotCount(vm) == 2) {
        code = wrenGetSlotDouble(vm, 1);
    }

    exit(code);
}

static void stdout_write(WrenVM* vm)
{
    const char* str = wrenGetSlotString(vm, 1);
    fprintf(stdout, "%s", str);
}

static void stderr_write(WrenVM* vm)
{
    const char* str = wrenGetSlotString(vm, 1);
    fprintf(stderr, "%s", str);
}

NestClass classes[] = {
    {"Environment", {NULL, NULL}},
    {"System", {NULL, NULL}},
    {"Stdout", {NULL, NULL}},
    {"Stderr", {NULL, NULL}},
    {NULL, {NULL, NULL}}
};

NestMethod methods[] = {
    {"Environment", "get(_)", true, environment_get},
    {"Environment", "set(_,_)", true, environment_set},
    {"Environment", "unset(_)", true, environment_unset},

    {"System", "gc()", true, system_gc},
    {"System", "exit(_)", true, system_exit},
    {"System", "exit()", true, system_exit},

    {"Stdout", "write(_)", false, stdout_write},

    {"Stderr", "write(_)", false, stderr_write},
    {NULL, NULL, false, NULL},
};

NestLibrary nest_system_init()
{
    NestLibrary lib = {
        classes,
        methods
    };

    return lib;
}

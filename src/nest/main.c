#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <sysexits.h>
#include "file.h"
#include "nestCore.h"
#include "module.h"
#include "vm.h"

#define HELP 'h'
#define VERSION 'v'
#define COMMAND 'c'

/**
 * Prints the help message text
 */
static inline void help()
{
    printf("Usage: wren [file] [arguments...]\n");
    printf("  -h     Show this help\n");
    printf("  -v     Show program version\n");
    printf("  -c cmd Run program passed in as string\n");
}

/**
 * Prints the version text
 */
static inline void version()
{
    printf("wren %s\n", WREN_VERSION_STRING);
}

static WrenVM* prepareNest()
{
    prepareWrenIncludePath();
    initialiseNest();
    return initialiseVM();
}

static char* readSourceFile(const char* path) 
{
    if (!fileExists(path)) {
        fprintf(stderr, "Provided file does not exist, %s", path);
        return NULL; 
    }   
  
    char* source = NULL;
  
    if ((source = readFile(path)) == NULL) {
        fprintf(stderr, "Could not open provided file, %s", path);
        return NULL; 
    }

    return source;
}

int main(int argc, char** argv)
{
    if (argc == 1) {
        help();
        return EXIT_SUCCESS;
    }

    int c;
    char* input = NULL;

    while ((c = getopt(argc, argv, "+hvc:")) != -1) {
        switch (c) {
            case HELP:
                help();
                return EXIT_SUCCESS;
            case VERSION:
                version();
                return EXIT_SUCCESS;
            case COMMAND:
                input = optarg;
                break;
            case '?':
                return EX_USAGE;
            default:
                return EXIT_FAILURE;
        }
    }

    if (input == NULL) {
        if ((input = readSourceFile(argv[1])) == NULL) {
            return EX_NOINPUT;
        }
    }

    WrenVM* vm = prepareNest();
    int status = interpretSource(vm, input);
    finaliseVM(vm);
    freeWrenIncludePath();
    finaliseNest();

    return status;
}

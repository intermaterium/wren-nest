#include <stdlib.h>
#include <dlfcn.h>
#include <stdio.h>
#include "nestCore.h"
#include "file.h"
#include "module.h"
#include "strlib.h"
#include "hashMap.h"

/**
 * The environment key for the include path
 */
static const char* INCLUDE_PATH_ENV_KEY = "WREN_PATH"; 

/**
 * The default include path, if we do not have one in the environment
 */
static const char* DEFAULT_INCLUDE_PATH = ":wren_modules:/usr/local/lib/wren";

/**
 * The include path for the interpreter
 */
static char** includePath = NULL;

/**
 * Gets the include path from the shells environment or use the default compiled
 * one
 */
static const char* getIncludePath()
{
    const char* path = getenv(INCLUDE_PATH_ENV_KEY);
    
    if (path != NULL) {
        return path;
    }

    return DEFAULT_INCLUDE_PATH;
}

void prepareWrenIncludePath()
{
    const char* path = getIncludePath();
    char** arr = strcsplit(path, ":");
    includePath = arr;
}

void freeWrenIncludePath()
{
    if (includePath == NULL) {
        return;
    }

    for (int i = 0; includePath[i] != NULL; i++) {
        free(includePath[i]);
    }

    free(includePath);
}

char* resolveWrenModule(const char* restrict module, const char* restrict extension)
{
    /* If the module starts with /, . or .., it's a absolute or relative search, use the provided path */
    if (module[0] == '/' || strncmp(module, "./", 2) == 0 || strncmp(module, "../", 3) == 0) {
        char* path = buildString("%s.%s", module, extension);
        if (fileExists(path)) {
            return path;
        }

        free(path);
        return NULL;
    }

    for (int i = 0; includePath[i] != NULL; i++) {
        char* path = NULL;
        const char* part = includePath[i];

        if (strisempty(part)) {
            path = buildString("%s.%s", module, extension);
        } else {
            path = buildString("%s/%s.%s", part, module, extension); 
        }

        if (fileExists(path)) {
            return path;
        }

        free(path);
    }
        
    return NULL;
}

void linkLibrary(
    const char* restrict path,
    const char* restrict module,
    NestLibrary* lib
) {
    void* handle = NULL;
    char* error;

    if ((handle = dlopen(path, RTLD_LAZY)) == NULL) {
        fprintf(stderr, "Failed to import wren module %s. Reason: %s\n", module, dlerror());
        exit(EXIT_FAILURE);
    }

    dlerror();    /* Clear any existing error */
    
    char* func = buildString("nest_%s_init", module);

    NestLibrary(*init)() = dlsym(handle, func);

    if (init == NULL) {
        dlclose(handle);
        fprintf(
            stderr,
            "Failed to import wren module %s. Reason: Missing init function %s\n",
            module,
            func
        );
        free(func);
        exit(EXIT_FAILURE);
    }

    if ((error = dlerror()) != NULL)  {
        dlclose(handle);
        free(func);
        fprintf(stderr, "Failed to import wren module %s. Reason: %s\n", module, error);
        exit(EXIT_FAILURE);
    }

    free(func);
    *lib = (*init)();
}

void resolveWrenCModule(const char* restrict path, const char* restrict module)
{
    /* If the file does not exist, don't try to import anything */
    if (!fileExists(path)) {
        return;
    }

    NestLibrary lib;
    
    linkLibrary(path, module, &lib);

    for (int i = 0; lib.classes[i].name != NULL; i++) {
        nestRegisterClass(module, &lib.classes[i]);
    }

    for (int i = 0; lib.methods[i].className != NULL; i++) {
        nestRegisterMethod(module, &lib.methods[i]);
    }
}

#ifndef WREN_MODULE_H
#define WREN_MODULE_H

#include "wren.h"
#include <stdbool.h>

/**
 * Sets up the include path for the wren cli
 */
void prepareWrenIncludePath();

/**
 * Frees the stored include path
 */
void freeWrenIncludePath();

/**
 * Resolves a wren module on the file system
 */
char* resolveWrenModule(const char* restrict module, const char* restrict extension);

/**
 * Resolves a wren c module on the file system
 */
void resolveWrenCModule(const char* restrict path, const char* restrict module);

WrenForeignMethodFn resolveWrenMethod(
    const char* restrict module,
    const char* restrict class,
    bool isStatic,
    const char* restrict signature
);

WrenForeignClassMethods resolveWrenClass(
    const char* restrict module,
    const char* restrict classNamei
);

#endif

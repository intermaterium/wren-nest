#include "nestCore.h"
#include "hashMap.h"
#include "strlib.h"
#include <stdio.h>

static struct HashMap* nestClasses = NULL;
static struct HashMap* nestMethods = NULL;

void initialiseNest()
{
    nestClasses = hashMapNew();
    nestMethods = hashMapNew();
}

int nestRegisterClass(
    const char* restrict module,
    NestClass* nestClass
) {
    char* symbol = buildString("%s@%s", module, nestClass->name);

    if (symbol == NULL) {
        return -1;
    }

    WrenForeignClassMethods* methods = malloc(sizeof(WrenForeignClassMethods));
    
    if (methods == NULL) {
        free(symbol);
        return -1;
    }

    memcpy(methods, &nestClass->methods, sizeof(WrenForeignClassMethods));

    int res = hashMapInsert(nestClasses, symbol, methods);
    free(symbol);
    return res;
}

WrenForeignClassMethods nestResolveClass(
    const char* restrict module,
    const char* restrict className
) {
    WrenForeignClassMethods methods = {NULL, NULL};

    char* symbol = buildString("%s@%s", module, className);

    if (symbol == NULL) {
        return methods;
    }

    WrenForeignClassMethods* search = hashMapSearch(nestClasses, symbol);

    if (search != NULL) {
        methods.allocate = search->allocate;
        methods.finalize = search->finalize;
    }

    free(symbol);
    return methods;
}

int nestRegisterMethod(
    const char* restrict module,
    NestMethod* method
) {
    char* symbol = buildString(
        "%s@%s@%s@%d",
        module,
        method->className,
        method->signature,
        method->isStatic
    );

    if (symbol == NULL) {
        return -1;
    }

    WrenForeignMethodFn func = malloc(sizeof(WrenForeignMethodFn));

    if (func == NULL) {
        free(symbol);
        return -1;
    }

    func = method->func;

    const int res = hashMapInsert(nestMethods, symbol, func);
    free(symbol);
    return res;
}

WrenForeignMethodFn nestResolveMethod(
    const char* restrict module,
    const char* restrict className,
    const char* restrict signature,
    bool isStatic
) {
    char* symbol = buildString(
        "%s@%s@%s@%d",
        module,
        className,
        signature,
        isStatic
    );

    if (symbol == NULL) {
        return NULL;
    }

    WrenForeignMethodFn func = hashMapSearch(nestMethods, symbol);

    if (func == NULL) {
        printf("Function is null\n");
    }
    free(symbol);
    return func;
}

void finaliseNest()
{
    hashMapFree(nestClasses);
    hashMapFree(nestMethods);
}

#ifndef NEST_CORE_H
#define NEST_CORE_H

#include <wren.h>
#include "nest.h"

void initialiseNest();

int nestRegisterClass(
    const char* restrict module,
    NestClass* nestClass
);

WrenForeignClassMethods nestResolveClass(
    const char* restrict module,
    const char* restrict className
);

int nestRegisterMethod(
    const char* restrict module,
    NestMethod* method
);

WrenForeignMethodFn nestResolveMethod(
    const char* restrict module,
    const char* restrict className,
    const char* restrict signature,
    bool isStatic
);

void finaliseNest();

#endif

#include <stdio.h>
#include "vm.h"
#include "file.h"
#include "module.h"
#include "nestCore.h"
#include "strlib.h"

/**
 * Function called when wren attempts to import a module
 */
static char* importModule(WrenVM* vm, const char* module)
{
    char* path = resolveWrenModule(module, "wren");

    if (path == NULL) {
        return NULL;
    }

    char* source = readFile(path);

    if (source == NULL) {
        free(path);
        return NULL;
    }

    char* lib = strreplace(path, ".wren", ".so");

    resolveWrenCModule(lib, module);
    free(path);
    free(lib);

    return source;
}

static WrenForeignMethodFn resolveForeignMethod(
    WrenVM* vm,
    const char* module,
    const char* className,
    bool isStatic,
    const char* signature
) {
    return nestResolveMethod(module, className, signature, isStatic);
}

WrenForeignClassMethods resolveForeignClass(
    WrenVM* vm,
    const char* module,
    const char* className
) {
    return nestResolveClass(module, className);
}

/**
 * Function called when wren encounters an error
 */
static void reportError(
    WrenVM* vm,
    WrenErrorType type,
    const char* module,
    int line,
    const char* message
) {
    switch (type) {
        case WREN_ERROR_COMPILE:
            fprintf(stderr, "[%s line %d] %s\n", module, line, message);
            break;
        case WREN_ERROR_RUNTIME:
            fprintf(stderr, "%s\n", message);
            break;
        case WREN_ERROR_STACK_TRACE:
            fprintf(stderr, "[%s line %d] in %s\n", module, line, message);
            break;
    }
}

/**
 * Function to initialise the wren interpreter
 */
WrenVM* initialiseVM()
{
    WrenConfiguration config;
    wrenInitConfiguration(&config);
    config.errorFn = reportError;
    config.loadModuleFn = importModule;
    config.bindForeignMethodFn = resolveForeignMethod;
    config.bindForeignClassFn = resolveForeignClass;

    return wrenNewVM(&config);
}

void finaliseVM(WrenVM* vm)
{
    wrenFreeVM(vm);
}

int interpretSource(WrenVM* vm, const char* source)
{
    WrenInterpretResult result = wrenInterpret(
        vm,
        "main",
        source
    );

    switch (result) {
        case WREN_RESULT_COMPILE_ERROR:
            return -1;
        case WREN_RESULT_RUNTIME_ERROR:
            return -1;
        case WREN_RESULT_SUCCESS:
            return 0;
    }

    return 0;
}

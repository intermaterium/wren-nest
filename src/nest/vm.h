#ifndef WREN_VM_H
#define WREN_VM_H

#include "wren.h"

/**
 * Creates a new wren virtual machine and sets it's internal variables
 */
WrenVM* initialiseVM();

/**
 *  Frees the wren virtual machine
 */
void finaliseVM(WrenVM* vm);

/**
 * Interprets the wren source code
 */
int interpretSource(WrenVM* vm, const char* source);

#endif
